import React from "react";
import PropTypes from "prop-types";
import Head from "../components/Head";

const Home = props => <Head title="Homepage" />;

export default Home;
