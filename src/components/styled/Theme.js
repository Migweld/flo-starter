const Theme = {
  // Palette
  white: "#fff",
  black: "#000",

  breakpoints: {
    xs: 320,
    s: 480,
    m: 750,
    l: 960,
    xl: 1280,
    xxl: 1440
  }
};

export default Theme;
