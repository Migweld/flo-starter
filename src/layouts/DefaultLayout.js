import React from "react";
import { ThemeProvider } from "styled-components";
import Theme from "../components/styled/Theme";

const DefaultLayout = ({ children }) => (
  <ThemeProvider theme={Theme}>{children}</ThemeProvider>
);

export default DefaultLayout;
