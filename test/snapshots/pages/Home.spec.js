import React from "react";
import "jest-styled-components";
import renderer from "react-test-renderer";
import Home from "../../../src/pages/index";

describe("Snapshot tests", () => {
  test("should render correctly", () => {
    let tree = renderer.create(<Home />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
