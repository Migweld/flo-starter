import calcEm from "../../lib/calcEm";

describe("calcEm tests", () => {
  test("should return correct ems for pixel value", () => {
    let result = calcEm(16);
    expect(result).toEqual(1);
    result = calcEm(10);
    expect(result).toEqual(0.625);
  });
});
