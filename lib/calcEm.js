const calcEm = (px, baseUnit = 16) => {
  if (px === 1) {
    return 0.0725;
  }
  return (px / baseUnit) * 1;
};

export default calcEm;
