module.exports = {
  transform: {
    "^.+\\.jsx?$": "<rootDir>/jest-preprocess.js"
  },
  setupFiles: [],
  testPathIgnorePatterns: ["<rootDir>/node_modules/", ".cache/"],
  transformIgnorePatterns: ["node_modules/(?!(gatsby)/)"],
  setupFiles: ["<rootDir>/jest.setup.js", "<rootDir>/loadershim.js"],
  setupFilesAfterEnv: ["<rootDir>/setup-test-env.js"]
};
